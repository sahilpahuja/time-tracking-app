import React from "react";
import EditableTimerList from "./EditableTimerList";
import ToggleableTimerForm from "./ToggleableTimerForm";
import Helper from "../helper";
import Client from "../client";
import timer from "./Timer";

class TimersDashboard extends React.Component {
    state = {
        timers: []
    };

    componentDidMount() {
        this.loadTimersFromServer();
        setInterval(this.loadTimersFromServer, 5000);
    }

    loadTimersFromServer = () => {
        Client.getTimers((serverResponse) => {
            this.setState({timers: serverResponse});
        });
    }

    handleCreateFormSubmit = (timer) => {
        this.createTimer(timer);
    }

    handleEditFormSubmit = (timer) => {
        this.updateTImer(timer);
    }

    handleTrashClick = (timerId) => {
        this.deleteTimer(timerId);
    }

    handleStartClick = (timerId) => {
        this.startTimer(timerId);
    }

    handleStopClick = (timerId) => {
        this.stopTimer(timerId);
    }

    createTimer = (timer) => {
        const t = Helper.newTimer(timer);
        this.setState({timers: this.state.timers.concat(t) });
        Client.createTimer(t);
    }

    updateTImer = (updatedTImer) => {
        this.setState({
            timers: this.state.timers.map((timer) => {
                if (timer.id === updatedTImer.id) {
                    return Object.assign({}, timer, {
                        title: updatedTImer.title,
                        project: updatedTImer.project,
                    });
                }
                return timer;
            }),
        });
        Client.updateTimer(timer);
    }

    deleteTimer = (timerId) => {
        this.setState({
            timers: this.state.timers.filter(timer => timer.id !== timerId)
        });
        Client.deleteTimer({ id: timerId });
    }

    startTimer = (timerId) => {
        const now = Date.now();

        this.setState({
            timers: this.state.timers.map(timer => {
                if (timer.id === timerId) {
                    return Object.assign({}, timer, {
                        runningSince: now
                    });
                }
                return timer;
            }),
        });
        Client.startTimer({
            id: timerId,
            start: now,
        });
    }

    stopTimer = (timerId) => {
        const now = Date.now();

        this.setState({
            timers: this.state.timers.map(timer => {
                if (timer.id === timerId) {
                    const lastElapsed = now - timer.runningSince;
                    return Object.assign({}, timer, {
                        elapsed: timer.elapsed + lastElapsed,
                        runningSince: null
                    });
                }
                return timer;
            }),
        });

        Client.stopTimer({
            id: timerId,
            stop: now,
        });
    }

    render() {
        return (
            <div className='ui three column centered grid'>
                <div className="column">
                    <EditableTimerList
                        timers={this.state.timers}
                        onFormSubmit={this.handleEditFormSubmit}
                        onTrashClick={this.handleTrashClick}
                        onStartClick={this.handleStartClick}
                        onStopClick={this.handleStopClick}
                    />
                    <ToggleableTimerForm
                        onFormSubmit = {this.handleCreateFormSubmit}
                    />
                </div>
            </div>
        );
    }
}

export default TimersDashboard;